---
tags:
  - securité
  - vault
---

## C'est quoi Vault ?

Vault est un système de gestion des secrets et du cryptage basé sur l'identité. Un secret est tout ce dont vous voulez contrôler étroitement l'accès, comme les clés de chiffrement de l'API, les mots de passe ou les certificats. Vault fournit des services de chiffrement qui sont contrôlés par des méthodes d'authentification et d'autorisation. 

Un système moderne nécessite l'accès à une multitude de secrets : informations d'identification de la base de données, clés API pour les services externes, informations d'identification pour la communication de l'architecture orientée services

Les principales caractéristiques de Vault sont :  

- [x] Secure Secret Storage  

    > Des secrets arbitraires de type clé/valeur peuvent être stockés dans Vault. Vault chiffre ces secrets avant de les écrire sur le stockage persistant, de sorte qu'il ne suffit pas d'accéder au stockage brut pour accéder à vos secrets. Vault peut écrire sur le disque, sur Consul, et plus encore  

- [x] Dynamic Secrets  

    > Vault peut générer des secrets à la demande pour certains systèmes, comme AWS ou les bases de données SQL. Par exemple, lorsqu'une application doit accéder à un bucket S3, elle demande à Vault des informations d'identification, et Vault génère à la demande une paire de clés AWS avec des autorisations valides. Après avoir créé ces secrets dynamiques, Vault les révoque aussi automatiquement à l'expiration du bail.

- [x] Data Encryption  

    > Vault peut chiffrer et déchiffrer des données sans les stocker. Cela permet de définir des paramètres de chiffrage et aux développeurs de stocker des données chiffrées dans un emplacement tel qu'une base de données SQL sans avoir à concevoir leurs propres méthodes de cryptage.

- [x] Leasing and Renewal

    > Tous les secrets dans Vault sont associés à un bail. À la fin du bail, Vault révoque automatiquement ce secret. Les clients peuvent renouveler les baux via les API de renouvellement intégrées.

- [x] Revocation

    > Vault a un support intégré pour la révocation des secrets. Vault peut révoquer non seulement des secrets uniques, mais aussi un arbre de secrets, par exemple tous les secrets lus par un utilisateur spécifique, ou tous les secrets d'un type particulier. La révocation facilite le roulement des clés ainsi que le verrouillage des systèmes en cas d'intrusion.

## Cas d'utilisations

## Aperçu de l'architecture
