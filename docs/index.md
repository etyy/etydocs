---
hide:
  - navigation
  - toc
---
# Welcome :material-open-source-initiative:

<figure markdown>
  ![Napoléon](assets/images/napoleon.png){ width="280" }
  <figcaption>All the documentation is in french</figcaption>
  [:fontawesome-solid-paper-plane:](mailto:etienne@loutsch.me){ .md-button .md-button--primary .center }
  [:fontawesome-brands-linkedin-in:](https://www.linkedin.com/in/etienneloutsch/){ .md-button .md-button--primary center }  

  Build with [MkDocs](https://squidfunk.github.io/mkdocs-material/) on Gitlab [Pages](https://docs.gitlab.com/ee/user/project/pages/)
</figure>
